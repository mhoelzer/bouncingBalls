// setup canvas
// references canvas
const canvas = document.querySelector("canvas");
// gives us a context on which we can start to draw; drawOnCanvas = object that directly represents the drawing area of the canvas and lets us to draw 2D shapes on it
const drawOnCanvas = canvas.getContext("2d");
const width = canvas.width = window.innerWidth;
const height = canvas.height = window.innerHeight;

// function to generate random number; two numbers as arguments, and returns a random number in the range between the two
function random(min, max) {
    let number = Math.floor(Math.random() * (max - min)) + min;
    return number;
}


// properties of balls; this is a constructor 
function Ball(x, y, xVelocity, yVelocity, color, size) {
    // coordinates; vert/horz to say where ball will start; ranges from 0 (topleft) to wdith/height of of vp (botright)
    this.x = x;
    this.y = y;
    // horz/vert velocity; will regularly be added to x and y coords when animate to move them by so much in each frame
    this.xVelocity = xVelocity;
    this.yVelocity = yVelocity;
    // gets color
    this.color = color;
    // gets size; in radius in px
    this.size = size;

};

// tells ball to draw itself onto screen by calling series of memvers of drawOnCanvas
Ball.prototype.draw = function() {
    // want to draw shape
    drawOnCanvas.beginPath();
    // define what color 
    drawOnCanvas.fillStyle = this.color;
    // trace arc shape; x/y of arc center/specify x/y; radius of arc/ball size; start and end nums of degrees round the circle arc is drawn between (2*PI is 360 in radians for full circle; 1*PI is 180)
    drawOnCanvas.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    // finish drawing path we started in beginPath and add the color
    drawOnCanvas.fill();
}


Ball.prototype.changePosition = function() {
    /* checks to see if ball has reached edge of canvas; if yes, reverse polarity of relevant vel to make bvall go opposite dir (if ball goes up (+yVelocity) the yVelocity becomes negative to travel down)
    need size b/c x/y are center of ball but want edge to bounce off canvas */
    // is x > width of canvas/goes off right edge
    if((this.x + this.size) > width) {
        this.xVelocity = -(this.xVelocity);
    }
    // is x < 0/goes off left edge
    if((this.x - this.size) <= 0) {
        this.xVelocity = -(this.xVelocity);
    }
    // is y < height/goes off bottom
    if((this.y + this.size) >= height) {
        this.yVelocity = -(this.yVelocity);
    }
    // is y > 0/goes off top
    if((this.y - this.size) <= 0) {
        this.yVelocity = -(this.yVelocity);
    }

    // add xVel/yVel to x and y to move ball each time this method is called
    this.x += this.xVelocity;
    this.y += this.yVelocity;
};

Ball.prototype.detectCollisions = function() {
    // go through all balls to see if any ball has collided w/ the others
    for (let ball = 0; ball < ballStorage.length; ball++) {
        // is curr ball being looped through same ball as current check?; no collide w/ self, so !
        if(!(this === ballStorage[ball])) {
            // do any areas of circles touch/overlap
            let dx = this.x - ballStorage[ball].x;
            let dy = this.y - ballStorage[ball].y;
            let distance = Math.sqrt(dx * dx + dy * dy);
            // if yes collide, set color of both to new random
            if(distance < this.size + ballStorage[ball].size) {
                ballStorage[ball].color = this.color = `rgb(${random(0, 255)},${random(0, 255)},${random(0, 255)}`;
                let sound = new Audio('bounceQuiet.mp3');
                sound.play();
            }
        }
    }
}


// ball storage
const ballStorage = [];

function moveBalls() {
    // make semitrans black rectangle of canvas to see little trails
    drawOnCanvas.fillStyle = "rgba(0, 0, 0, .25";
    // covers previous frame's drawing so no snakes 
    drawOnCanvas.fillRect(0, 0, width, height);

    // only 25 balls on screen
    while (ballStorage.length < 15) {
        let size = random(10, 20);
        let ball = new Ball(
            // ball position always drawn at least one ball width
            // away from the edge of the canvas to avoid drawing erros
            random(0 + size, width - size),
            random(0 + size, height - size),
            random(-7, 7),
            random(-7, 7),
            `rgb(${random(0, 255)},${random(0, 255)},${random(0, 255)}`,
            size
        );
        // puts ball at end of stroage
        ballStorage.push(ball);
    }

    for (let counter = 0; counter < ballStorage.length; counter++) {
        ballStorage[counter].draw();
        ballStorage[counter].changePosition();
        ballStorage[counter].detectCollisions();
    }

    // runs function again set rumber of times/second for smooth; usually done recursively so it calls itself everytime run 
    requestAnimationFrame(moveBalls);
}
moveBalls();

